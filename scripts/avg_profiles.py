#!/usr/bin/env python

import re
import sys
import glob
import string
import numpy as np

################################################################################################
########################################## MAIN PROGRAM ########################################
################################################################################################

# Command line arguments
# sys.argv[0]  = program itself
# sys.argv[1:] = input profiles 

# Show usage
if (len(sys.argv) < 2) or ('-h' in sys.argv[1:]) or ('--help' in sys.argv[1:]):
    msg  = '\n'
    msg += '----------------------------------------------------------------------\n'
    msg += 'Description: This module takes a list of pressure and density profiles\n'
    msg += '             in XVG format and prints the average profile in the same\n'
    msg += '             format\n\n'
    msg += 'Usage:  avg_profiles.py [input_profiles] > [average_profile]\n\n'
    msg += '        [input_profiles]  = list of XVG files or filename pattern\n'
    msg += '        [average_profile] = XVG file\n'
    msg += '----------------------------------------------------------------------\n'
    sys.exit(msg)

# Initialize variables
num_profs  = 0
lab_profs  = []
if len(sys.argv) > 2:
    xvg_files = map(str, sys.argv[1:])
else:
    xvg_files = sorted(glob.glob(sys.argv[1])) 

# Read profiles
for xvg in xvg_files:

    lb     =  0
    nl     = -1
    read   = True
    values = []

    pf    = open(xvg, 'r')
    lines = pf.readlines()
    pf.close() 

    # Read lines
    while read and nl < len(lines)-1:
        nl += 1 
        ln  = lines[nl]

        # Read title
        if re.search('@\s*title', ln):
            title = ln.strip()

        # Read xaxis
        elif re.search('@\s*xaxis', ln):
            xaxis = ln.strip()

        # Read yaxis
        elif re.search('@\s*yaxis', ln):
            yaxis = ln.strip()

        # Read labels
        elif re.search('@\s*s[0-9]+', ln):
            label = re.search('\"(.+)\"', ln).group(1)
            if lb < len(lab_profs) and label != lab_profs[lb]:
                read = False
                sys.stderr.write("\nERROR: Read label ({0:s}) != ".format(label))
                sys.stderr.write("Previous label ({0:s}) ".format(lab_profs[lb]))
                sys.stderr.write("in position {0:d}\n".format(lb))
                sys.stderr.write("       Skipping data loading for {0:s}\n\n".format(xvg))
            elif lb > len(lab_profs):
                read = False
                sys.stderr.write("\nERROR: Number of read labels ({0:d}) > ".format(lb+1))
                sys.stderr.write("Number of previous labels ({0:s}) ".format(len(lab_profs)))
                sys.stderr.write("in position {0:d}\n".format(lb))
                sys.stderr.write("       Skipping data loading for {0:s}\n\n".format(xvg))
            else:
                if lb == len(lab_profs) and (num_profs == 0):
                    lab_profs.append(label)
                lb += 1                

        # Read profiles values
        elif not re.search('^\s*@|^\s*#', ln):
            pv = map(float, ln.split())
            if len(pv) != len(lab_profs)+1:
                read = False
                sys.stderr.write("\nERROR: Number of data columns ({0:d}) != ".format(len(pv)))
                sys.stderr.write("Number of labels + 1 ({0:d})\n".format(len(lab_profs)+1))
                sys.stderr.write("       Skipping data loading for {0:s}\n\n".format(xvg))
            else:
                values.append(pv)

    # Update sum profiles
    if read:
        arr_profs = np.array(values)
        if num_profs == 0:
            sys.stderr.write("Initializing sum profile for {0:s} ...\n".format(xvg)) 
            sum_profs = arr_profs
            num_profs  += 1 
        elif arr_profs.shape == sum_profs.shape:
            sys.stderr.write("Updating sum profile for {0:s} ...\n".format(xvg) )
            sum_profs = np.add(sum_profs, arr_profs)
            num_profs  += 1 
        else:
            arow, acol = arr_profs.shape
            srow, scol = sum_profs.shape 
            sys.stderr.write("\nERROR: Current profile size ({0:d},{1:d}) != ".format(arow, acol))
            sys.stderr.write("Sum profile size ({0:d},{1:d})\n".format(srow, scol))
            sys.stderr.write("       Skipping profile update for {0:s}\n\n".format(xvg))               
            
# Print headers
print title
print xaxis
print yaxis

# Print labels
for n, lab in enumerate(lab_profs):
    print "@ s{0:d} legend \"{1:s}\"".format(n, lab)

# Print data
for row in sum_profs:
    sout = ''
    for d in row:
        sout += "{0:15.4f}".format(d/num_profs)
    print sout  

