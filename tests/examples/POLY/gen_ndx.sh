# Auxiliar function to write atoms in ndx file with 10 columns formatting
write_ndx () {
    NDXNAME=$1
    NDXLIST=$2
    COUNT=1
    echo "[ $NDXNAME ]"
    for ATOM in $NDXLIST; do
        if [ $COUNT -lt 10 ]; then
            printf "%8s " "$ATOM"
            COUNT=$((COUNT+1))
        else
            printf "%8s\n" "$ATOM"
            COUNT=1
        fi
    done
    if [ $COUNT -gt 1 ]; then 
        echo -e "\n"
    fi
}

# Axiliar function to show usage
usage() {
    echo -e "usage: gen_ndx.sh [options]\n"
    echo -e "OPTIONS:
         -i  Input GRO file
         -o  Output NDX file\n"
}

# Options parser
while getopts "i:o:" OPTION; do
    case $OPTION in
        i)
            GRO=$OPTARG;;
        o)
            NDX=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z $GRO || -z $NDX ]]; then
     usage; exit 1
fi

# Residue name
RESN='PLY'
HEAD='EO'
TAIL='BD'

# Determine head and tail atoms
HEADATOMS=$(cat $GRO  | tail -n +3 | awk -v rn=$RESN -v hd=$HEAD  '{if ($1 ~ rn && $2 ~ hd) print NR}')
TAILATOMS=$(cat $GRO  | tail -n +3 | awk -v rn=$RESN -v tl=$TAIL  '{if ($1 ~ rn && $2 ~ tl) print NR}')

# Generate index file with default groups
echo 'q' | gmx make_ndx -quiet -f $GRO -o $NDX

# Add new groups
write_ndx 'PEO' "$HEADATOMS" >> $NDX
write_ndx 'PBD' "$TAILATOMS" >> $NDX

