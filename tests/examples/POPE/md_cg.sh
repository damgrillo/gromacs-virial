#!/bin/bash

#######################################################################################
###################################### USER SETTINGS ##################################
#######################################################################################

# Set BINDIR, WORKDIR and JOBDIR (full paths to avoid further issues)
WORKDIR=$(cd $(dirname $0) && pwd)
GMXDIR=$HOME/Software/gromacs-3DLS

# Source GROMACS
source $GMXDIR/bin/GMXRC
export GMX_MAXBACKUP=-1

# System parameters 
PREFIX=POPE-water-pw
NPTTYPE=NPT-semi
TEMP=300
TSTEP=0.02
NSTEP=50000
NJOBS=1
NFMAX=1

# PDB and ITP files
MOLDIR=$WORKDIR/mol
LIPD_ITP=$MOLDIR/martini/martini_v2.0_lipids.itp
MART_ITP=$MOLDIR/martini/martini_v2.2P_alltypes.itp

# Set list of ITP files to be included 
ITPLIST="$MART_ITP $LIPD_ITP"

#######################################################################################
#######################################################################################
#######################################################################################

# Set dirs
INPDIR=$WORKDIR/memb/inp
OUTDIR=$WORKDIR/memb/out
TMPDIR=$WORKDIR/tmp
mkdir -p $TMPDIR
cd $TMPDIR

# Set input files
MDP=$INPDIR/$PREFIX.mdp
GRO=$INPDIR/$PREFIX.gro
NDX=$INPDIR/$PREFIX.ndx
TOP=$INPDIR/$PREFIX.top
TPR=$INPDIR/$PREFIX.tpr

# Generate NDX and TOP files
echo q | gmx make_ndx -quiet -f $GRO -o $NDX 
$WORKDIR/gen_top.sh  -i $GRO -t $TOP -p "$ITPLIST" -n "$PREFIX"

##########################################################################


# Set last job number
LJOB=$(ls $OUTDIR/$PREFIX | grep -o 'job-[0-9]\+\.log' | grep -o '[0-9]\+' | sort | tail -1)
if [ -z "$LJOB" ]; then
    FIRST=0000
else

    # Check if last job completed successfully
    LOG=$WORKDIR/memb/$JOBDIR/out/$PREFIX/job-$LJOB.log
    STRFIN=$(cat $LOG | tail | grep 'Finished mdrun')
    STRTER=$(cat $LOG | grep 'Received' | grep 'TERM')
    STRINT=$(cat $LOG | grep 'Received' | grep 'INT')
    if [[ -n "$STRFIN" && -z "$STRTER" && -z "$STRINT" ]]; then JOBCOMP=1; else JOBCOMP=0; fi

    # If job completed, move first to next job, else stay in last job
    if [ $JOBCOMP -eq 1 ]; then
        FIRST=$(echo "$LJOB + 1" | bc | awk '{printf "%04g", $0}')
    else
        FIRST=$LJOB
    fi
fi

# Run program
NFAILS=0
JOBNUM=$(printf "%04g" "$FIRST")
while [ $JOBNUM -lt $NJOBS ]; do
    TRR=$OUTDIR/job-$JOBNUM.trr
    XTC=$OUTDIR/job-$JOBNUM.xtc
    EDR=$OUTDIR/job-$JOBNUM.edr
    LOG=$OUTDIR/job-$JOBNUM.log
    RESLIST=$(cat $GRO | tail -n +3 | head -n -1 | awk '{print $1}' | sed 's/^[0-9]\+//g' | sort | uniq | tr '\n' ' ')  

    $WORKDIR/gen_mdp_cg.sh -m $MDP -t $TEMP -e $NPTTYPE -l $TSTEP -s $NSTEP -r "$RESLIST" -f 0                      
    gmx grompp -quiet -f $MDP -c $GRO -p $TOP -o $TPR
    gmx mdrun -v -s $TPR -c $GRO -o $TRR -x $XTC -e $EDR -g $LOG 

    # Check if job completed successfully
    STRFIN=$(cat $LOG | tail | grep 'Finished mdrun')
    STRTER=$(cat $LOG | grep 'Received' | grep 'TERM')
    STRINT=$(cat $LOG | grep 'Received' | grep 'INT')
    if [[ -n "$STRFIN" && -z "$STRTER" && -z "$STRINT" ]]; then JOBCOMP=1; else JOBCOMP=0; fi

    # If job completed, calculate properties, move to next job and put NFAILS to 0, else remove job files and put NFAILS +1
    if [ $JOBCOMP -eq 1 ]; then
        JOBNUM=$(echo "$JOBNUM + 1" | bc | awk '{printf "%04g", $0}')
        NFAILS=0
    else
        NFAILS=$((NFAILS+1))
        rm $XTC $EDR $LOG
    fi

    # If NFAILS == NFMAX, exit program due to many consecutive failures
    if [ $NFAILS -eq $NFMAX ]; then
        echo -e "\nToo many consecutive fails ($NFAILS), due to abnormal termination." 
        echo -e "Probable causes may be:"
        echo -e "* Hardware stability issues (e.g. node failure when running in clusters)"
        echo -e "* System stability issues (e.g. system entrapped in unstable configuration)"
        echo -e "* MD run parameters issues (e.g. timestep too large for current system state)"
        echo -e "* Other unknown issue"
        echo -e "Aborting ...\n"
        exit 1
    fi

done

rm -r $WORKDIR/tmp 


